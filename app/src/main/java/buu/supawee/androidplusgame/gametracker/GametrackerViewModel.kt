package buu.supawee.androidplusgame.gametracker

import android.app.Application
import android.text.Editable
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import buu.supawee.androidplusgame.database.Game
import buu.supawee.androidplusgame.database.GameDatabaseDao
import kotlinx.coroutines.*

class GametrackerViewModel (dataSource: GameDatabaseDao, application: Application): ViewModel(){
    val database = dataSource
    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private var currentGame = MutableLiveData<Game?>()
    val games = database.getUsers()

    private var _username = MutableLiveData<String>()
    val username: LiveData<String>
        get() = _username

    private var _eventSubmit = MutableLiveData<Boolean?>()
    val eventSubmit: LiveData<Boolean?>
        get() = _eventSubmit

    fun usernameUpdate(getName: Editable){
        _username.value = getName.toString()
    }
    private val _navigateToGame = MutableLiveData<Game>()
    val navigateToGame: LiveData<Game>
        get() = _navigateToGame

    fun doneNavigating() {
        _navigateToGame.value = null
    }

    init{
        initializeGame()
    }

    private fun initializeGame(){
        uiScope.launch {
            currentGame.value = getGameNowFromDatabase()
        }
    }

    private suspend fun getGameNowFromDatabase():Game?{
        return withContext(Dispatchers.IO){
            var now = database.getUser()
            if(now?.userName?.length == 0){
                now = null
            }
            now
        }
    }

    private suspend fun insert(game: Game) {
        withContext(Dispatchers.IO) {
            database.insert(game)
        }
    }

    private suspend fun update(game: Game) {
        withContext(Dispatchers.IO) {
            database.update(game)
        }
    }

    private suspend fun clear() {
        withContext(Dispatchers.IO) {
            database.clear()
        }
    }

    fun onStart() {
        _eventSubmit.value = true

        uiScope.launch {
            val newGame = Game()
            newGame.userName = _username.value.toString()
            insert(newGame)
            currentGame.value = getGameNowFromDatabase()

            _navigateToGame.value = newGame
            currentGame.value = null
        }
    }

    fun onClear() {
        uiScope.launch {
            clear()
            currentGame.value = null
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}