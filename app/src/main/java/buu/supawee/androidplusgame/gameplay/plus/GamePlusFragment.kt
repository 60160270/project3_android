package buu.supawee.androidplusgame.gameplay.plus

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import buu.supawee.androidplusgame.R
import buu.supawee.androidplusgame.database.GameDatabase
import buu.supawee.androidplusgame.databinding.FragmentGamePlusBinding
import buu.supawee.androidplusgame.gameplay.plus.GamePlusFragmentArgs
import buu.supawee.androidplusgame.gameplay.plus.GamePlusFragmentDirections
import buu.supawee.androidplusgame.gameplay.GameViewModelFactory

class GamePlusFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentGamePlusBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_game_plus,container,false)
        val application = requireNotNull(this.activity).application
        val arguments =
            GamePlusFragmentArgs.fromBundle(
                arguments
            )

        val dataSource = GameDatabase.getInstance(application).gameDatabaseDao
        val viewModelFactory =
            GamePlusViewModelFactory(
                arguments.gameId,
                dataSource
            )

        val gamePlusViewModel = ViewModelProvider(this, viewModelFactory).get(GamePlusViewModel::class.java)

        binding.gamePlusViewModel = gamePlusViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        gamePlusViewModel.navigateToGametracker.observe(viewLifecycleOwner, Observer { game ->
            game?.let{
                this.findNavController().navigate(GamePlusFragmentDirections.actionGamePlusFragmentToGametrackerFragment())
                gamePlusViewModel.doneNavigating()
            }
        })

        return binding.root
    }
}