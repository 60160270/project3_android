package buu.supawee.androidplusgame.gameplay.minus

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import buu.supawee.androidplusgame.R
import buu.supawee.androidplusgame.database.GameDatabase
import buu.supawee.androidplusgame.databinding.FragmentGameMinusBinding

class GameMinusFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentGameMinusBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_game_minus,container,false)
        val application = requireNotNull(this.activity).application
        val arguments =
            GameMinusFragmentArgs.fromBundle(
                arguments
            )

        val dataSource = GameDatabase.getInstance(application).gameDatabaseDao
        val viewModelFactory =
            GameMinusViewModelFactory(
                arguments.gameId,
                dataSource
            )

        val gameMinusViewModel = ViewModelProvider(this, viewModelFactory).get(GameMinusViewModel::class.java)

        binding.gameMinusViewModel = gameMinusViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        gameMinusViewModel.navigateToGametracker.observe(viewLifecycleOwner, Observer { game ->
            game?.let{
                this.findNavController().navigate(GameMinusFragmentDirections.actionGameMinusFragmentToGametrackerFragment())
                gameMinusViewModel.doneNavigating()
            }
        })

        return binding.root
    }
}