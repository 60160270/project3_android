package buu.supawee.androidplusgame.gameplay

import android.provider.SyncStateContract.Helpers.update
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import buu.supawee.androidplusgame.database.Game
import buu.supawee.androidplusgame.database.GameDatabaseDao
import kotlinx.coroutines.*
import javax.sql.CommonDataSource

class GameViewModel(private val gameKey: Long = 0L, dataSource: GameDatabaseDao) : ViewModel() {
    val database = dataSource
    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val _navigateToGametracker = MutableLiveData<Boolean?>()
    private var currentGame = MutableLiveData<Game?>()

    val navigateToGametracker: LiveData<Boolean?>
        get() = _navigateToGametracker

    private val _navigateToPlusGame = MutableLiveData<Game>()
    val navigateToPlusGame: LiveData<Game>
        get() = _navigateToPlusGame

    private val _navigateToMinusGame = MutableLiveData<Game>()
    val navigateToMinusGame: LiveData<Game>
        get() = _navigateToMinusGame

    private val _navigateToMultipleGame = MutableLiveData<Game>()
    val navigateToMultipleGame: LiveData<Game>
        get() = _navigateToMultipleGame

    init{
        initializeGame()
    }
    private fun initializeGame(){
        uiScope.launch {
            currentGame.value = getGameNowFromDatabase()
        }
    }

    private suspend fun getGameNowFromDatabase():Game?{
        return withContext(Dispatchers.IO){
            var now = database.getUser()
            if(now?.userName?.length == 0){
                now = null
            }
            now
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun doneNavigating(){
        _navigateToGametracker.value = null
        _navigateToMinusGame.value = null
        _navigateToMultipleGame.value = null
        _navigateToPlusGame.value = null
    }

    fun onBack(){
        _navigateToGametracker.value = true
    }

    fun onPlus(){
        uiScope.launch {
            Log.i("test","GameViewModel currentGame.value ${currentGame.value}")
            val oldUser = currentGame.value ?: return@launch
            _navigateToPlusGame.value = oldUser

            currentGame.value = null
        }
    }
    fun onMinus(){
        uiScope.launch {
            Log.i("test","GameViewModel currentGame.value ${currentGame.value}")
            val oldUser = currentGame.value ?: return@launch
            _navigateToMinusGame.value = oldUser

            currentGame.value = null
        }
    }
    fun onMultiple(){
        uiScope.launch {
            Log.i("test","GameViewModel currentGame.value ${currentGame.value}")
            val oldUser = currentGame.value ?: return@launch
            _navigateToMultipleGame.value = oldUser

            currentGame.value = null
        }
    }

}