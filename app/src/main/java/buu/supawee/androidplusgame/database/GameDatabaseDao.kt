package buu.supawee.androidplusgame.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface GameDatabaseDao {
    @Insert
    fun insert(game:Game)

    @Update
    fun update(game:Game)

    @Query("SELECT * from game_list WHERE gameId = :key")
    fun get(key:Long): Game

    @Query("DELETE FROM game_list")
    fun clear()

    @Query("SELECT * FROM game_list ORDER BY gameId DESC")
    fun getUsers(): LiveData<List<Game>>

    @Query("SELECT * FROM game_list ORDER BY user_score DESC")
    fun getRankUsers(): LiveData<List<Game>>

    @Query("SELECT * FROM game_list ORDER BY gameId Desc LIMIT 1")
    fun getUser():Game?
}