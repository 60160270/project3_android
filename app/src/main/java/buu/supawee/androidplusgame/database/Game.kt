package buu.supawee.androidplusgame.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "game_list")
data class Game(
    @PrimaryKey(autoGenerate = true)
    var gameId: Long = 0L,
    @ColumnInfo(name = "user_name")
    var userName: String =  "",
    @ColumnInfo(name = "user_score")
    var gameScore: Int = 0
)