package buu.supawee.androidplusgame.gameplay.plus

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import buu.supawee.androidplusgame.database.Game
import buu.supawee.androidplusgame.database.GameDatabaseDao
import kotlinx.coroutines.*
import kotlin.Int as Int

class GamePlusViewModel(private val gameKey: Long = 0L, dataSource: GameDatabaseDao) : ViewModel() {
    val database = dataSource
    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val _navigateToGametracker = MutableLiveData<Boolean?>()
    private var currentGame = MutableLiveData<Game?>()

    //game variable
    private var _variable = MutableLiveData<Int>()
    private val variable: LiveData<Int>
        get() {
            return _variable
        }

    private var _variable2 = MutableLiveData<Int>()
    private val variable2: LiveData<Int>
        get() {
            return _variable2
        }
    private var _resultCorrect = MutableLiveData<Int>()
    val resultCorrect: LiveData<Int>
        get(){
            return _resultCorrect
        }

    private var _choice = MutableLiveData<Int>()
    val choice: LiveData<Int>
        get() {
            return _choice
        }
    private var _choice2 = MutableLiveData<Int>()
    val choice2: LiveData<Int>
        get() {
            return _choice2
        }
    private var _choice3 = MutableLiveData<Int>()
    val choice3: LiveData<Int>
        get() {
            return _choice3
        }
    private var _choice4 = MutableLiveData<Int>()
    val choice4: LiveData<Int>
        get() {
            return _choice4
        }
    private var _score = MutableLiveData<Int>()
    val score: LiveData<Int>
        get() {
            return _score
        }
    private var _question = MutableLiveData<String>()
    val question: LiveData<String>
        get(){
            return _question
        }
    //end game variable

    val navigateToGametracker: LiveData<Boolean?>
        get() = _navigateToGametracker

    init{
        initializeGame()
    }
    private fun initializeGame(){
        uiScope.launch {
            currentGame.value = getGameNowFromDatabase()
            _score.value = currentGame.value?.gameScore
            Log.i("test","PlusGame currentGame.value ${currentGame.value}")
        }
    }

    private suspend fun getGameNowFromDatabase():Game?{
        return withContext(Dispatchers.IO){
            var now = database.getUser()
            if(now?.userName?.length == 0){
                now = null
            }
            now
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun doneNavigating(){
        _navigateToGametracker.value = null
    }

    fun onBack(){
        _navigateToGametracker.value = true
    }

    private suspend fun update(game: Game) {
        withContext(Dispatchers.IO) {
            database.update(game)
        }
    }

    // game plus code
    private fun makeResult(): Int {
        _variable.value = (0..10).random()
        _variable2.value = (0..10).random()
        _question.value = "${variable.value} + ${variable2.value} = ?"
        return _variable.value!! + _variable2.value!!
    }
    private fun resetAns(){
        _resultCorrect.value = makeResult()
    }
    private fun setAns(){
        var choice = (1..4).random()
        when(choice){
            1 -> {
                _choice.value = _resultCorrect.value
                _choice2.value = _resultCorrect.value!!.plus(1)
                _choice3.value = _resultCorrect.value!!.plus(2)
                _choice4.value = _resultCorrect.value!!.plus(3)
            }
            2 -> {
                _choice.value = _resultCorrect.value!!.minus(1)
                _choice2.value = _resultCorrect.value
                _choice3.value = _resultCorrect.value!!.plus(1)
                _choice4.value = _resultCorrect.value!!.plus(2)
            }
            3 -> {
                _choice.value = _resultCorrect.value!!.minus(2)
                _choice2.value = _resultCorrect.value!!.minus(1)
                _choice3.value = _resultCorrect.value
                _choice4.value = _resultCorrect.value!!.plus(1)
            }
            4 -> {
                _choice.value = _resultCorrect.value!!.minus(3)
                _choice2.value = _resultCorrect.value!!.minus(2)
                _choice3.value = _resultCorrect.value!!.minus(1)
                _choice4.value = _resultCorrect.value
            }
        }
    }
    fun checkChoice(){
        if(_choice.value == _resultCorrect.value) choiceCorrect() else choiceIncorrect()
    }
    fun checkChoice2(){
        if(_choice2.value == _resultCorrect.value) choiceCorrect() else choiceIncorrect()
    }
    fun checkChoice3(){
        if(_choice3.value == _resultCorrect.value) choiceCorrect() else choiceIncorrect()
    }
    fun checkChoice4(){
        if(_choice4.value == _resultCorrect.value) choiceCorrect() else choiceIncorrect()
    }
    private fun choiceCorrect(){
        uiScope.launch {
            onSetGameScore(1)
            nextQuestion()
        }
    }
    private fun choiceIncorrect(){
        nextQuestion()
    }
    private fun nextQuestion(){
        resetAns()
        setAns()
    }

    private suspend fun onSetGameScore(point: Int){
        uiScope.launch {
            withContext(Dispatchers.IO) {
                val thisGame = database.get(gameKey)
                _score.postValue((score.value?.plus(point)))
                thisGame.gameScore = score.value!!
                database.update(thisGame)
                Log.i("test","Updated thisGame = $thisGame")
            }
        }
    }

    init {
        nextQuestion()
    }
}