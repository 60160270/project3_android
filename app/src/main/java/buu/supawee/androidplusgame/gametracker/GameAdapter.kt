package buu.supawee.androidplusgame.gametracker

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import buu.supawee.androidplusgame.R
import buu.supawee.androidplusgame.database.Game

class GameAdapter : RecyclerView.Adapter<GameAdapter.ViewHolder>(){
    var data = listOf<Game>()
        set(value){
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item,data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameAdapter.ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView){
        private val userName: TextView = itemView.findViewById(R.id.game_user_name)
        private val userScore: TextView = itemView.findViewById(R.id.game_user_score)
        private val rankImage: ImageView = itemView.findViewById(R.id.game_user_rank)

        fun bind(
            item: Game,
            data: List<Game>
        ) {
            userName.text = item.userName
            userScore.text = item.gameScore.toString()
            Log.i("debug","${data.indexOf(item)}")
            rankImage.setImageResource(when (data.indexOf(item)){
                0 -> R.drawable.crown
                1 -> R.drawable.medal
                2 -> R.drawable.medal2
                else -> R.drawable.ic_launcher_foreground
            })
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.list_item_game, parent, false)

                return ViewHolder(view)
            }
        }
    }
}