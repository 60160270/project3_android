package buu.supawee.androidplusgame.gameplay.plus

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.supawee.androidplusgame.database.GameDatabaseDao

class GamePlusViewModelFactory (
    private val gameKey: Long,
    private val dataSource: GameDatabaseDao) : ViewModelProvider.Factory
{
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GamePlusViewModel::class.java)) {
            return GamePlusViewModel(
                gameKey,
                dataSource
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}