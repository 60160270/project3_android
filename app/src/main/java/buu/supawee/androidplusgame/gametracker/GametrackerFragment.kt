package buu.supawee.androidplusgame.gametracker

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import buu.supawee.androidplusgame.R
import buu.supawee.androidplusgame.database.GameDatabase
import buu.supawee.androidplusgame.databinding.FragmentGametrackerBinding
import kotlinx.android.synthetic.main.fragment_gametracker.*

class GametrackerFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentGametrackerBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_gametracker, container, false)
        val application = requireNotNull(this.activity).application
        val dataSource = GameDatabase.getInstance(application).gameDatabaseDao
        val viewModelFactory = GametrackerFactory(dataSource, application)
        val gameTrackerViewModel = ViewModelProvider(this,viewModelFactory).get(GametrackerViewModel::class.java)

        binding.gameTrackerViewModel = gameTrackerViewModel
        binding.lifecycleOwner = this

        gameTrackerViewModel.eventSubmit.observe(viewLifecycleOwner, Observer { hasClick ->
            if(hasClick!!){
                gameTrackerViewModel.usernameUpdate(txtName.text)
                hideKeyBoard()
            }
        })

        gameTrackerViewModel.navigateToGame.observe(viewLifecycleOwner, Observer { game ->
            game?.let{
                this.findNavController().navigate(GametrackerFragmentDirections.actionGametrackerFragmentToGameFragment(game.gameId))
                gameTrackerViewModel.doneNavigating()
            }
        })

        val adapter = GameAdapter()
        gameTrackerViewModel.games.observe(viewLifecycleOwner, Observer{
            it?.let{
                adapter.data = it
            }
        })

        binding.gameLists.adapter = adapter
        return binding.root
    }
    private fun hideKeyBoard(){
        val inputMethodManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view?.windowToken,0)
    }

}