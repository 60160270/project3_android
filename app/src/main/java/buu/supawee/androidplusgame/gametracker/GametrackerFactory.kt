package buu.supawee.androidplusgame.gametracker

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.supawee.androidplusgame.database.GameDatabaseDao

class GametrackerFactory(
    private val dataSource: GameDatabaseDao,
    private val application: Application) : ViewModelProvider.Factory {
        @Suppress("unchecked_cast")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(GametrackerViewModel::class.java)) {
                return GametrackerViewModel(dataSource, application) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
}