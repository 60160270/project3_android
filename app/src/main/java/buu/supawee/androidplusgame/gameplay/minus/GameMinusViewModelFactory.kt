package buu.supawee.androidplusgame.gameplay.minus

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.supawee.androidplusgame.database.GameDatabaseDao

class GameMinusViewModelFactory (
    private val gameKey: Long,
    private val dataSource: GameDatabaseDao) : ViewModelProvider.Factory
{
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GameMinusViewModel::class.java)) {
            return GameMinusViewModel(
                gameKey,
                dataSource
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}