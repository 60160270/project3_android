package buu.supawee.androidplusgame.gameplay

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import buu.supawee.androidplusgame.R
import buu.supawee.androidplusgame.database.GameDatabase
import buu.supawee.androidplusgame.databinding.FragmentGameBinding

class GameFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentGameBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_game,container,false)
        val application = requireNotNull(this.activity).application
        val arguments = GameFragmentArgs.fromBundle(arguments)

        val dataSource = GameDatabase.getInstance(application).gameDatabaseDao
        val viewModelFactory = GameViewModelFactory(arguments.gameId,dataSource)

        val gameViewModel = ViewModelProvider(this, viewModelFactory).get(GameViewModel::class.java)

        binding.gameViewModel = gameViewModel

        gameViewModel.navigateToGametracker.observe(viewLifecycleOwner, Observer { game ->
            game?.let{
                this.findNavController().navigate(GameFragmentDirections.actionGameFragmentToGametrackerFragment())
                gameViewModel.doneNavigating()
            }
        })

        gameViewModel.navigateToPlusGame.observe(viewLifecycleOwner, Observer { game ->
            game?.let{
                this.findNavController().navigate(GameFragmentDirections.actionGameFragmentToGamePlusFragment(game.gameId))
                gameViewModel.doneNavigating()
            }
        })
        
        gameViewModel.navigateToMinusGame.observe(viewLifecycleOwner, Observer { game ->
            game?.let{
                this.findNavController().navigate(GameFragmentDirections.actionGameFragmentToGameMinusFragment(game.gameId))
                gameViewModel.doneNavigating()
            }
        })

        gameViewModel.navigateToMultipleGame.observe(viewLifecycleOwner, Observer { game ->
            game?.let{
                this.findNavController().navigate(GameFragmentDirections.actionGameFragmentToGameMultipleFragment(game.gameId))
                gameViewModel.doneNavigating()
            }
        })

        return binding.root
    }
}