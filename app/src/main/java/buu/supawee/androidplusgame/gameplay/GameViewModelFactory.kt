package buu.supawee.androidplusgame.gameplay

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.supawee.androidplusgame.database.GameDatabaseDao
import buu.supawee.androidplusgame.gameplay.plus.GamePlusViewModel

class GameViewModelFactory (
    private val gameKey: Long,
    private val dataSource: GameDatabaseDao) : ViewModelProvider.Factory
    {
        @Suppress("unchecked_cast")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(GameViewModel::class.java)) {
                return GameViewModel(gameKey, dataSource) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }