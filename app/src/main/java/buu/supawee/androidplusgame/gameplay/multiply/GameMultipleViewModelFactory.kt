package buu.supawee.androidplusgame.gameplay.multiply

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.supawee.androidplusgame.database.GameDatabaseDao

class GameMultipleViewModelFactory (
    private val gameKey: Long,
    private val dataSource: GameDatabaseDao) : ViewModelProvider.Factory
{
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GameMultipleViewModel::class.java)) {
            return GameMultipleViewModel(
                gameKey,
                dataSource
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}