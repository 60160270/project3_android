package buu.supawee.androidplusgame.gameplay.multiply

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import buu.supawee.androidplusgame.R
import buu.supawee.androidplusgame.database.GameDatabase
import buu.supawee.androidplusgame.databinding.FragmentGameMinusBinding
import buu.supawee.androidplusgame.databinding.FragmentGameMultipleBinding

class GameMultipleFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentGameMultipleBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_game_multiple,container,false)
        val application = requireNotNull(this.activity).application
        val arguments =
            GameMultipleFragmentArgs.fromBundle(
                arguments
            )

        val dataSource = GameDatabase.getInstance(application).gameDatabaseDao
        val viewModelFactory =
            GameMultipleViewModelFactory(
                arguments.gameId,
                dataSource
            )

        val gameMultipleViewModel = ViewModelProvider(this, viewModelFactory).get(GameMultipleViewModel::class.java)

        binding.gameMultipleViewModel = gameMultipleViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        gameMultipleViewModel.navigateToGametracker.observe(viewLifecycleOwner, Observer { game ->
            game?.let{
                this.findNavController().navigate(GameMultipleFragmentDirections.actionGameMultipleFragmentToGametrackerFragment())
                gameMultipleViewModel.doneNavigating()
            }
        })

        return binding.root
    }
}